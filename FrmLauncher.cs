﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using TechnicalTools.UI;



namespace TechnicalTools.Deployment
{
    public partial class FrmLauncher : Form
    {
        readonly Deployer _deployer;
        readonly Filter   _filter;
        

        [Obsolete("For designer only")]
        protected FrmLauncher()
            : this(null, null)
        {
        }

        public FrmLauncher(Deployer deployer, Filter filter)
        {
            _deployer = deployer;
            _filter = filter;
            _filter.CompleteWithDefault();

            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            Opacity = 0;
            Width = 1;
            Height = 1;
            Left = -Width;
            Top = -Height;

            Text = Path.GetFileName(_deployer.Config.LauncherExecutableName);
            if (_deployer.Config.ApplicationIcon != null)
                Icon = _deployer.Config.ApplicationIcon;

            //_folder = GetArgument(args, "folder") ?? _folder;
            //_domain = GetArgument(args, "domain") == null ? _domain : (eEnvironment)Enum.Parse(typeof(eEnvironment), GetArgument(args, "domain"), true);
            //_envName = GetArgument(args, "envName") ?? _envName;
        }

        void FrmLauncher_Load(object sender, EventArgs e)
        {
            var path = _deployer.FindBestVersionExePathOnDiskFor(_filter, out string bestConsistentRelease);
            if (!string.IsNullOrWhiteSpace(path))
            {
                _deployer.StartVersion(path, _filter.Domain.Value, _filter.EnvName);
                Environment.Exit(0);
            }

            if (bestConsistentRelease != null)
            {
                var frm = new FrmDeployer(_deployer, _filter);
                Hide();
                frm.ShowDialog();
            }
            else
                MessageBox.Show($"No release of {_deployer.Config.ApplicationName} found!, ask {_deployer.Config.ApplicationName} Deployer tool to IT");

            Close();
        }

        
    }
}
