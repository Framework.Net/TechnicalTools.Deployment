﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using TechnicalTools.Automation;
using TechnicalTools.Deployment.Data;


namespace TechnicalTools.Deployment
{
    public class Config
    {
        [Argument(IsMandatory = false,
                  Description = "Specify the connection string that first dialog with database to know more about domain and environment and to let user to authenticate.",
                  DefaultValue = nameof(eEnvironment.Dev),
                  AllowedValueRange = nameof(eEnvironment.Prod) + ", " + nameof(eEnvironment.Dev) + ", " + nameof(eEnvironment.PreProd) + ", " + nameof(eEnvironment.Demo) + ", " + nameof(eEnvironment.Sandbox) + ", " + nameof(eEnvironment.LocalNoDB))]
        public virtual string InitialConnectionString
        {
            get { return _initialConnectionString; }
            set
            {
                _initialConnectionString = value;
                DataAccessor = null;
            }
        }
        string _initialConnectionString;
        public virtual string DefaultInitConnectionString { get { return null; } }

        public virtual IDataAccessor DataAccessor
        {
            get { return _dataAccessor ?? (DataAccessor = new DefaultDataAccessor(this)); }
            set { _dataAccessor = value; }
        }
        IDataAccessor _dataAccessor;

        public virtual string CompanyName { get; set; }
        public virtual string ApplicationName { get; set; }
        public virtual string ApplicationExecutableName { get; set; }
        public virtual string LauncherExecutableName    { get; set; }
        public virtual string DeployerExecutableName    { get; set; }
        public virtual string LauncherRelativePathToBinFromMainProjectOutputFolder { get; set; }

        public virtual string DeployRepository
        {
            get
            {
                if (_DeployRepository != null)
                    return _DeployRepository;
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                                    CompanyName, ApplicationName);
            }
            set { _DeployRepository = value; }
        } string _DeployRepository;
        public virtual string ManifestFileName { get; } = "manifest.txt";

        /// <summary>Can be Null</summary>
        public virtual Icon ApplicationIcon { get; set; }
        
        protected static Icon GetIconFromCallingAssembly(Type programType, string filename)
        {
            using (var stream = programType.Assembly.GetManifestResourceStream(programType.Namespace + "." + filename))
            {
                Debug.Assert(stream != null, nameof(stream) + " != null");
                return new Icon(stream);
            }
        }
    }
}
