﻿using System;

using TechnicalTools.Deployment.Data;


namespace TechnicalTools.Deployment
{
    public class Filter
    {
        public eEnvironment? Domain  { get; set; }
        public string        EnvName { get; set; }

        public bool IsEmpty  { get { return Domain == null && EnvName == null; } }
        public bool IsFilled { get { return !IsEmpty; } }

        public static readonly Filter Default = new Filter()
        {
            Domain = eEnvironment.Prod,
            EnvName = "Default"
        };
        public static readonly Filter DefaultDev = new Filter()
        {
            Domain = eEnvironment.Dev,
            EnvName = "Default"
        };

        public void CompleteWithDefault()
        {
            Domain = Domain ?? Default.Domain;
            if (string.IsNullOrWhiteSpace(EnvName))
                EnvName = Default.EnvName;
        }
    }

}
