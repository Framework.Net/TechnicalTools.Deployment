﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using TechnicalTools.Model;
using TechnicalTools.UI;


namespace TechnicalTools.Deployment
{
    public partial class FrmDeployer : Form
    {
        readonly Deployer _deployer;
        readonly Filter _filter;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CreateShortCut { get { return chkInstallShortCutOnDesktop.Checked; } set { chkInstallShortCutOnDesktop.Checked = value; } }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool RunAfterInstall { get { return chkRunAfterInstall.Checked; } set { chkRunAfterInstall.Checked = value; } }

        public event Action InstallDone;

        [Obsolete("For designer only", true)]
        protected FrmDeployer()
            : this(null, null)
        {
        }

        public FrmDeployer(Deployer deployer, Filter filter, string login = null)
        {
            _deployer = deployer;
            _filter = filter;
            _filter.CompleteWithDefault();

            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            if (_deployer.Config.ApplicationIcon != null)
                Icon = _deployer.Config.ApplicationIcon;

            Text = Path.GetFileNameWithoutExtension(_deployer.Config.DeployerExecutableName);
            chkRunAfterInstall.Text = chkRunAfterInstall.Text.Replace("<your app>", deployer.Config.ApplicationName);
            chkInstallShortCutOnDesktop.Text = chkInstallShortCutOnDesktop.Text.Replace("<your app>", deployer.Config.ApplicationName);

            txtLogin.Text = login ??
                            (!Environment.UserName.Contains(" ") ? Environment.UserName
                          : Environment.UserName.Remove(Environment.UserName.IndexOf(' ')).ToLowerInvariant()[0]
                          + Environment.UserName.Substring(1 + Environment.UserName.IndexOf(' ')).ToLowerInvariant());
            lblLogs.Text = string.Empty;
            txtDomain.Text = _filter.Domain.ToString();
            txtEnvName.Text = _filter.EnvName;
        }

        void btnInstall_Click(object sender, EventArgs e)
        {
            Install(txtLogin.Text.Trim(), txtPassword.Text.Trim()).ConfigureAwait(false);
        }

        public async Task Install(string login, string password)
        {
            string text = btnInstall.Text;
            try
            {
                btnInstall.Enabled = false;
                txtLogin.Enabled = false;
                txtPassword.Enabled = false;

                btnInstall.Text = "Installing, please wait...";
                var installSHortcut = chkInstallShortCutOnDesktop.Checked;
                var runAfterInstall = chkRunAfterInstall.Checked;

                var progress = new Progress<string>();
                IProgress<string> pr = progress;
                progress.ProgressChanged += (_, msg) => BeginInvoke((Action)(() => lblLogs.Text = msg));
                string launcher = await Task.Run(() =>
                {
                    pr.Report("Authenticating...");
                    _deployer.Authenticate(login, password, _filter.Domain, _filter.EnvName);
                    pr.Report($"Getting available versions of {_deployer.Config.ApplicationName}.");
                    var versionToInstall = _deployer.GetVersionAllowedAndAvailable(_filter).FirstOrDefault();
                    if (versionToInstall == null)
                    {
                        pr.Report($"No version of {_deployer.Config.ApplicationName} is allowed for this environement.");
                        return null;
                    }
                    var res = _deployer.Install(versionToInstall, pr);
                    Debug.Assert(res != null);
                    return res;
                });
                if (launcher == null)
                    throw new TechnicalException("Version to install is not available!", null);
                Debug.Assert(_filter.Domain != null, "_filter.Domain != null");
                if (installSHortcut)
                {
                    _deployer.CreateDesktopShortcut(_filter.Domain.Value, _filter.EnvName, Path.GetDirectoryName(launcher));
                    MessageBox.Show(this, _deployer.Config.ApplicationName + " Shortcut to " + _filter.EnvName + " has been created on your Desktop!"
                                        + (runAfterInstall ? Environment.NewLine + "Click OK to run it" : ""));
                }
                var runNewInstall = runAfterInstall;
                InstallDone?.Invoke(); // This event could close "this"

                if (runNewInstall)
                {
                    Debug.Assert(File.Exists(launcher));
                    _deployer.StartVersion(_filter.Domain.Value, _filter.EnvName);
                }
            }
            catch (Exception ex)
            {
                btnInstall.Enabled = true;
                txtLogin.Enabled = true;
                txtPassword.Enabled = true;
                lblLogs.Text = ex.Message;
                MessageBox.Show(this, ex.Message, "Error :'(");
            }
            finally
            {
                btnInstall.Text = text;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Modal) DialogResult = DialogResult.Cancel;
            else Close();
        }


    }
}
