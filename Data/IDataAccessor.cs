﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Diagnostics;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    public interface IDataAccessor
    {
        List<T>           LoadCollection<T>()           where T : class, IAutoMappedDbObject;
        int               UpdateToDatabase<T>(T item)   where T : class, IAutoMappedDbObject;
        void              DeleteInDatabase<T>(T item)   where T : class, IAutoMappedDbObject;

        T                 CreateInstance<T>() where T : IDeploymentData;
        void              CreateInDatabase(IDeploymentData item);
        void              CreateInDatabaseCollection<T>(IReadOnlyCollection<T> items) where T : IDeploymentData;

        long              GetFileLength(ReleasedFile file);
        IEnumerable<int>  DownloadFileByChunk(ReleasedFile file, byte[] uniqueChunk, long totalLength);

        bool              IsAuthenticationRequired { get; }
        bool              IsAuthenticated          { get; set; }
        List<EnvironmentSetting> Authenticate(EnvironmentConfig env, string login ,string password);
    }

    public static class IDataAccessor_Extensions
    {
        public static readonly string FirstSettingReturnedIsBaseConnectionString = $"First setting returned by {nameof(IDataAccessor)}.{nameof(IDataAccessor.Authenticate)} must be the base connection string";

        public static List<EnvironmentConfig> GetAvailableEnvironments(this IDataAccessor accessor, Version app_version, Filter filter)
        {
            var now = DateTime.Now;
            var allEnvAllowed = accessor.LoadCollection<VersionAllowedByEnvironment>();
            var allowedEnvs = allEnvAllowed
                                .Where(ev => app_version == null ||
                                                (ev.Version_From == null || ev.Version_From <= app_version)
                                             && (ev.Version_To   == null || ev.Version_To >= app_version))
                                .Where(ev => ev.EffectiveFromDate == null || now >= ev.EffectiveFromDate)
                                .Where(ev => ev.EffectiveToDate   == null || now < ev.EffectiveToDate)
                                .OrderByDescending(ev => ev.EffectiveToDate)
                                .ThenByDescending(ev => ev.EffectiveFromDate)
                                .ToLookup(ev => ev.Environment_Id);

            var allEnvs = accessor.LoadCollection<EnvironmentConfig>();
            var envs = allEnvs
                        .Where(e => allowedEnvs.Contains(e.Id) 
                                 && (filter?.Domain  == null || e.Domain == filter.Domain.Value)
                                 && (filter?.EnvName == null || e.Name   == filter.EnvName))
                        .ToList();
            if (envs.Count == 0 && DebugTools.IsForDevelopper)
            {
                DebugTools.Break();
                // If you are here it is because your application needs to know on which environment to execute
                // At least one line must exist in Sql table "Deployment.Environments"
                // It is recommended to create it using domain Prod (see associated enum in the mapped class in DAL),
                // and name "Default"
                // I recomend you to read the "perfect long term fix".

                // Short term fix :
                //   If you dont want to bother much, you can force the first env (just for this execution), 
                //   by executing in Visual studio's Immediate Windows :
                //     envs = mapper.LoadCollection<EnvironmentConfig>().Skip(0).Take(1).ToList()
                //   Usually, the env you want is the first, otherwise change the skip / take calls as you need
                //   But the returning list must contains only one environment.

                // middle term fix :
                // Part 1
                //   A default environment should exist and point to production (Sql Table Deployment.Environments).
                //   So you have to use these arguments when running your app from Visual Studio:
                //     --domain=Prod --envName=Default // The values are from a line that should exist in SQL table Deployment.Environments by the way... (go see the mapping on the class)
                //   So, right click on your main application project (entry point) => Menu "Properties", then Tab "Debug", TextArea "Command Line Arguments"
                // Part 2
                //   Check also that the table Deployment.VersionAllowedByEnvironments allows you tu run the current version of your app
                //   The current version auto increase at each build (hour) 
                //   So you also need a line in table VersionAllowedByEnvironments
                //   where the current version of your app is inside the interval [Version_From, Version_To]
                //   You can put these two values to null in table to not being constrain your app to be in this interval to be allowed to run onthis environment...

                // Long term fix :
                //   Create a new line in table Deployment.Environments with setting : domain:DEV, Name: Default
                //   This environment will contains all the setting for developpers...
                //   Create another new line in table Deployment.Environments with setting : domain:DEV, Name: <your windows login name>, Inherited Env: the id of the previous env created
                //   So you have an environment just for you that inherit the setting of default shared environment
                //   Apply the "the middle term fix part 1" above using.
                //     --envName="%USERNAME%" --domain=Prod 
                //   Apply the "the middle term fix part 2" above to your new environment.


                // Perfect long term fix :
                //   Follow the long term fix above
                //   Add this argument to your app debug command line : 
                //     --AutoLoginWith=%USERNAME%   
                //   If the default connection string inside your app allows you only to authenticate (as expected),
                //   You will need to specify a connection string so that your app can use to read the schema user and find your login
                //     --InitialConnectionString="your connection string with added right for developper only"
                // Now when you run your app form visual studio, you autologin on your own environment and you share common developpement settings with all others of your team.
            }
            return envs;
        }
    }
}
