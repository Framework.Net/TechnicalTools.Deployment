﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IVersionAllowedByEnvironment : IDeploymentData
    {
             int  Id                { get; set; }
             int  Environment_Id    { get; set; }
         Version  Version_From      { get; set; }
         Version  Version_To        { get; set; }
        DateTime? EffectiveFromDate { get; set; }
        DateTime? EffectiveToDate   { get; set; }
    }
}
