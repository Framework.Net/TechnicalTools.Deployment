﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IReleasedFile : IDeploymentData, IComparable<IReleasedFile>, IComparable
    {
            byte Year      { get; set; }
            byte Month     { get; set; }
            byte Day       { get; set; }
            byte Hour      { get; set; }
        string Hash      { get; set; }
        string LocalPath { get; set; }

        Version Version { get; }
    }
}
