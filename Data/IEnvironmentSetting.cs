﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IEnvironmentSetting : IDeploymentData
    {
           int Environment_Id { get; set; }
        string FullKey        { get; set; }
        string Value          { get; set; }
        string Description    { get; set; }
    }
}
