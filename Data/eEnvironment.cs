﻿using System;
using System.ComponentModel;


namespace TechnicalTools.Deployment.Data
{
    public enum eEnvironment
    {
        [Description("Technical Base")]
        TechnicalBase = 0,
        Prod = 1,
        Dev = 2,
        [Description("Pre-Prod")]
        PreProd = 3,
        Demo = 4,
        Sandbox = 5,
        /// <summary> This kind of environnement is for working without connecting to a remote DB </summary>
        [Description("Local, No DB")]
        LocalNoDB = 6,
    }

}
