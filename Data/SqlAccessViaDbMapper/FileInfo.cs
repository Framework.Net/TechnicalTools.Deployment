﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [DbMappedTable(schemaName: "Deployment", tableName: "Files")]
    public class FileInfo : BaseDTO<IdTuple<string>>, IDeploymentData
    {
        [DebuggerHidden][DbMappedField("Hash", IsPK = true), DbMaxLength(32, MinimumLength = 32)] public string Hash          { [DebuggerStepThrough] get { return      _Hash; } [DebuggerStepThrough] set {   ChkFieldLen(ref value, 32); SetTechnicalProperty(ref  _Hash, value, RaiseIdChanged); } } string _Hash;
        [DebuggerHidden][DbMappedField("FileName"), DbMaxLength(256)]                             public string FileName      { [DebuggerStepThrough] get { return  _Filename; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 256);          SetProperty(ref              _Filename, value); } } string _Filename;

        protected override IdTuple<string> ClosedId
        {
            get { return new IdTuple<string>(Hash); }
            set { Hash = value.Id1; }
        }

        public FileInfo()
            : this(true)
        {
        }
        public FileInfo(bool initializeModelValues)
        {
            if (!initializeModelValues)
                return;
            _Hash = string.Empty;
            _Filename = string.Empty;
        }

        public new FileInfo Clone() { return (FileInfo)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new FileInfo(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (FileInfo)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés;
            _Hash = from._Hash;
            _Filename = from._Filename;
        }
    }
    public sealed class FileWithContent : FileInfo
    {
        [DebuggerHidden][DbMappedField("Content")] public byte[] Content   { [DebuggerStepThrough] get { return   _Content; } [DebuggerStepThrough] set {                                        SetProperty(ref               _Content, value); } } byte[] _Content;

        public FileWithContent()
            : this(true)
        {
        }
        public FileWithContent(bool initializeModelValues)
            : base(initializeModelValues)
        {
        }
    
        #region Cloneable
    
        public new FileWithContent Clone() { return (FileWithContent)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new FileWithContent(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            base.CopyAllFieldsFrom(source);
            var from = (FileWithContent)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés;
            _Content = from._Content;
        }
    
        #endregion
    }
}
