﻿using System;
using System.Diagnostics;

using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [DbMappedTable(schemaName: "Deployment", tableName: "ReleaseNotes")]
    public class ReleaseNote : BaseDTO<IdTuple<int>>, IDeploymentData
    {
        [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]      public     int Id            { [DebuggerStepThrough] get { return            _Id; } [DebuggerStepThrough] set {                              SetTechnicalProperty(ref            _Id, value, RaiseIdChanged); } }    int _Id;
        [DebuggerHidden][DbMappedField("Version"), DbMaxLength(50)] public Version Version       { [DebuggerStepThrough] get { return       _Version; } [DebuggerStepThrough] set {                              SetProperty         (ref       _Version, value);                 } } Version _Version;
        [DebuggerHidden][DbMappedField("IsPublic")]                 public    bool IsPublic      { [DebuggerStepThrough] get { return      _IsPublic; } [DebuggerStepThrough] set {                              SetProperty         (ref      _IsPublic, value);                 } }   bool _IsPublic;
        [DebuggerHidden][DbMappedField("Note")]                     public  string Note          { [DebuggerStepThrough] get { return          _Note; } [DebuggerStepThrough] set {                              SetProperty         (ref          _Note, value);                 } } string _Note;
        [DebuggerHidden][DbMappedField("NoteTechnical")]            public  string NoteTechnical { [DebuggerStepThrough] get { return _NoteTechnical; } [DebuggerStepThrough] set {                              SetProperty         (ref _NoteTechnical, value);                 } } string _NoteTechnical;
        [DebuggerHidden][DbMappedField("HasGIF")]                   public    bool HasGIF        { [DebuggerStepThrough] get { return        _HasGIF; } [DebuggerStepThrough] set {                              SetProperty         (ref        _HasGIF, value);                 } }   bool _HasGIF;
        
        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }

        #region Cloneable

        public new ReleaseNote Clone() { return (ReleaseNote)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new ReleaseNote(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (ReleaseNote)source;
            _Version =  from._Version;
            _IsPublic = from._IsPublic;
            _Note = from._Note;
        }

        #endregion

    }
    public sealed class ReleaseNoteWithContent : ReleaseNote, IUserInteractiveObject
    {
        [DebuggerHidden][DbMappedField("ExplainingGIF")]            public  byte[] ExplainingGIF { [DebuggerStepThrough] get { return _ExplainingGIF; } [DebuggerStepThrough] set {                              SetProperty         (ref _ExplainingGIF, value);                 } } byte[] _ExplainingGIF;

        #region Cloneable

        public new ReleaseNoteWithContent Clone() { return (ReleaseNoteWithContent)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new ReleaseNoteWithContent(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            base.CopyAllFieldsFrom(source);
            var from = (ReleaseNoteWithContent)source;
            _ExplainingGIF = from._ExplainingGIF;
        }

        #endregion
    }
}
