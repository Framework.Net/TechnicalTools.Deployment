﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [DbMappedTable(schemaName: "Deployment", tableName: "VersionAllowedByEnvironments")]
    [DebuggerDisplay("{" + nameof(DebugString) + ",nq}")]
    public sealed class VersionAllowedByEnvironment : BaseDTO<IdTuple<int>>, IDeploymentData
    {
        [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]                               public      int  Id                { [DebuggerStepThrough] get { return                 _Id; } [DebuggerStepThrough] set { SetTechnicalProperty(ref  _Id, value, RaiseIdChanged); } }       int _Id;
        [DebuggerHidden][DbMappedField("Environment_Id")]                                    public      int  Environment_Id    { [DebuggerStepThrough] get { return     _Environment_Id; } [DebuggerStepThrough] set {          SetProperty(ref      _Environment_Id, value); } }       int _Environment_Id;
        [DebuggerHidden][DbMappedField("Version_From", IsNullable = true), DbMaxLength(50)]  public  Version  Version_From      { [DebuggerStepThrough] get { return       _Version_From; } [DebuggerStepThrough] set {          SetProperty(ref        _Version_From, value); } }   Version _Version_From;
        [DebuggerHidden][DbMappedField("Version_To",   IsNullable = true), DbMaxLength(50)]  public  Version  Version_To        { [DebuggerStepThrough] get { return         _Version_To; } [DebuggerStepThrough] set {          SetProperty(ref          _Version_To, value); } }   Version _Version_To;
        [DebuggerHidden][DbMappedField("EffectiveFromDate", IsNullable = true)]              public DateTime? EffectiveFromDate { [DebuggerStepThrough] get { return  _EffectiveFromDate; } [DebuggerStepThrough] set {          SetProperty(ref   _EffectiveFromDate, value); } } DateTime? _EffectiveFromDate;
        [DebuggerHidden][DbMappedField("EffectiveToDate",   IsNullable = true)]              public DateTime? EffectiveToDate   { [DebuggerStepThrough] get { return    _EffectiveToDate; } [DebuggerStepThrough] set {          SetProperty(ref     _EffectiveToDate, value); } } DateTime? _EffectiveToDate;


        internal string DebugString
        {
            get
            {
                return nameof(Environment_Id) + "=" + Environment_Id + ", " +
                       nameof(Version_From) + "=" + (Version_From?.ToString() ?? "NULL") + ", " +
                       nameof(Version_To) + "=" + (Version_To?.ToString() ?? "NULL") + ", " +
                       nameof(EffectiveFromDate) + "=" + (EffectiveFromDate?.ToString() ?? "NULL") + ", " +
                       nameof(EffectiveToDate) + "=" + (EffectiveToDate?.ToString() ?? "NULL");
            }
        }

        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }

        #region Cloneable
    
        public new VersionAllowedByEnvironment Clone() { return (VersionAllowedByEnvironment)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new VersionAllowedByEnvironment(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (VersionAllowedByEnvironment)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés;
            _Environment_Id = from._Environment_Id;
            _Version_From = (Version)from._Version_From?.Clone();
            _Version_To = (Version)from._Version_To?.Clone();
            _EffectiveFromDate = from._EffectiveFromDate;
            _EffectiveToDate = from._EffectiveToDate;
        }
    
        #endregion
    }
}
