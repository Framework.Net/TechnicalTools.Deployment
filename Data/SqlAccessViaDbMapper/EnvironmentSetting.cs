﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [DbMappedTable(schemaName: "Deployment", tableName: "EnvironmentSettings")]
    [DebuggerDisplay("EnvId:{" + nameof(Environment_Id) + ",nq} | {" + nameof(FullKey) + ",nq}: {" + nameof(Value) + ",nq}")]
    public sealed class EnvironmentSetting : BaseDTO<IdTuple<int, string>>, IDeploymentData
    {
        [DebuggerHidden][DbMappedField("Environment_Id", IsPK = true, KeyOrderIndex = 1)]            public    int Environment_Id { [DebuggerStepThrough] get { return _Environment_Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Environment_Id, value, RaiseIdChanged); } }    int _Environment_Id;
        [DebuggerHidden][DbMappedField("FullKey", IsPK = true, KeyOrderIndex = 2), DbMaxLength(890)] public string FullKey        { [DebuggerStepThrough] get { return        _FullKey; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 890); SetTechnicalProperty(ref         _FullKey, value, RaiseIdChanged); } } string _FullKey;
        [DebuggerHidden][DbMappedField("Value", IsNullable = true)]                                  public string Value          { [DebuggerStepThrough] get { return          _Value; } [DebuggerStepThrough] set {                                        SetProperty(ref                           _Value, value); } } string _Value;
        [DebuggerHidden][DbMappedField("Description", IsNullable = true)]                            public string Description    { [DebuggerStepThrough] get { return    _Description; } [DebuggerStepThrough] set {                                        SetProperty(ref                     _Description, value); } } string _Description;

        public string ParentKey { get { var index = FullKey.LastIndexOf('.'); return index == -1 ? null    : FullKey.Remove(index); } }
        public string       Key { get { var index = FullKey.LastIndexOf('.'); return index == -1 ? FullKey : FullKey.Substring(index + 1); } }


        protected override IdTuple<int, string> ClosedId
        {
            get { return new IdTuple<int, string>(Environment_Id, FullKey); }
            set { Environment_Id = value.Id1; FullKey = value.Id2; }
        }

        #region Cloneable
    
        public new EnvironmentSetting Clone() { return (EnvironmentSetting)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new EnvironmentSetting(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (EnvironmentSetting)source;
            _Environment_Id = from._Environment_Id;
            _FullKey = from._FullKey;
            _Value = from._Value;
            _Description = from._Description;
        }

        #endregion
    }
}
