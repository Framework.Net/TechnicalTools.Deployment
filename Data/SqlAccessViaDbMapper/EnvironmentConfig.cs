﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [Serializable]
    [DbMappedTable(schemaName: "Deployment", tableName: "Environments")]
    [DebuggerDisplay("{" + nameof(Domain) + ",nq}" + " | {" + nameof(Name) + ",nq}")]
    public sealed class EnvironmentConfig : BaseDTO<IdTuple<int>>, IDeploymentData
    {
        [DebuggerHidden][DbMappedField("Id", IsAutoPK = true)]                        public    int Id                      { [DebuggerStepThrough] get { return                      _Id; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref       _Id, value, RaiseIdChanged); } }    int _Id;
        [DebuggerHidden][DbMappedField("Domain")]                                     public eEnvironment Domain            { [DebuggerStepThrough] get { return                  _Domain; } [DebuggerStepThrough] set {                                        SetProperty(ref                   _Domain, value); } } eEnvironment _Domain;
        [DebuggerHidden][DbMappedField("Name"), DbMaxLength(255)]                     public string Name                    { [DebuggerStepThrough] get { return                    _Name; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 255);          SetProperty(ref                     _Name, value); } } string _Name;
        [DebuggerHidden][DbMappedField("InheritedEnvironment_Id", IsNullable = true)] public   int? InheritedEnvironment_Id { [DebuggerStepThrough] get { return _InheritedEnvironment_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref  _InheritedEnvironment_Id, value); } }   int? _InheritedEnvironment_Id;
        [DebuggerHidden][DbMappedField("IsHiddenFromUser")]                           public   bool IsHiddenFromUser        { [DebuggerStepThrough] get { return        _IsHiddenFromUser; } [DebuggerStepThrough] set {                                        SetProperty(ref         _IsHiddenFromUser, value); } }   bool _IsHiddenFromUser;
            
        public string FullName { get { return Domain.GetDescription() + " - " + Name; } }

        protected override IdTuple<int> ClosedId
        {
            get { return new IdTuple<int>(Id); }
            set { Id = value.Id1; }
        }

        #pragma warning disable 809
        [Obsolete("Use Fullname property", true)]
        public override string ToString() { return FullName; }
        #pragma warning restore 809



        #region Cloneable

        public new EnvironmentConfig Clone() { return (EnvironmentConfig)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new EnvironmentConfig(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (EnvironmentConfig)source;
            // Note : Les champs appartenant à la PK ne sont pas copiés;
            _Domain = from._Domain;
            _Name = from._Name;
            _InheritedEnvironment_Id = from._InheritedEnvironment_Id;
            _IsHiddenFromUser = from._IsHiddenFromUser;
        }
    
        #endregion
    }
}
