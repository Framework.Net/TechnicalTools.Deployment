﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    [DbMappedTable(schemaName: "Deployment", tableName: "ReleasedFiles")]
    [DebuggerDisplay("{" + nameof(Version) + ",nq} {" + nameof(LocalPath) + ",nq}")]
    public sealed class ReleasedFile : BaseDTO<IdTuple<byte, byte, byte, byte, string, string>>, IDeploymentData, IComparable
    {
        [DebuggerHidden][DbMappedField("Year",  IsPK = true, KeyOrderIndex = 1)]                               public   byte Year      { [DebuggerStepThrough] get { return      _Year; } [DebuggerStepThrough] set {                                 SetTechnicalProperty(ref   _Year, value, RaiseIdChanged); } }   byte _Year;
        [DebuggerHidden][DbMappedField("Month", IsPK = true, KeyOrderIndex = 2)]                               public   byte Month     { [DebuggerStepThrough] get { return     _Month; } [DebuggerStepThrough] set {                                 SetTechnicalProperty(ref  _Month, value, RaiseIdChanged); } }   byte _Month;
        [DebuggerHidden][DbMappedField("Day",   IsPK = true, KeyOrderIndex = 3)]                               public   byte Day       { [DebuggerStepThrough] get { return       _Day; } [DebuggerStepThrough] set {                                 SetTechnicalProperty(ref    _Day, value, RaiseIdChanged); } }   byte _Day;
        [DebuggerHidden][DbMappedField("Hour",  IsPK = true, KeyOrderIndex = 4)]                               public   byte Hour      { [DebuggerStepThrough] get { return      _Hour; } [DebuggerStepThrough] set {                                 SetTechnicalProperty(ref   _Hour, value, RaiseIdChanged); } }   byte _Hour;
        [DebuggerHidden][DbMappedField("Hash",  IsPK = true, KeyOrderIndex = 5, FkTo = typeof(FileWithContent)), DbMaxLength(32, MinimumLength = 32)]
                                                                                                                public string Hash      { [DebuggerStepThrough] get { return      _Hash; } [DebuggerStepThrough] set { ChkFieldLen(ref value, 32, 32); SetTechnicalProperty(ref   _Hash, value, RaiseIdChanged); } } string _Hash;
        [DebuggerHidden][DbMappedField("LocalPath", KeyOrderIndex = 6), DbMaxLength(256)]                      public string LocalPath { [DebuggerStepThrough] get { return _LocalPath; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 256);                         SetProperty(ref _LocalPath, value); } } string _LocalPath;

        public Version Version { get { return new Version(Year, Month, Day, Hour); } }

        protected override IdTuple<byte, byte, byte, byte, string, string> ClosedId
        {
            get
            {
                return new IdTuple<byte, byte, byte, byte, string, string>(Year, Month, Day, Hour, Hash, LocalPath);
            }
            set
            {
                Year = value.Id1;
                Month = value.Id2;
                Day = value.Id3;
                Hour = value.Id4;
                Hash = value.Id5;
                LocalPath = value.Id6;
            }
        }


        public ReleasedFile()
            : this(true)
        {
        }
        public ReleasedFile(bool initializeModelValues)
        {
            if (!initializeModelValues)
                return;
            _Hash = string.Empty;
            _LocalPath = string.Empty;
        }

        public int CompareTo(ReleasedFile other)
        {
            var res = Version.CompareTo(other.Version);
            if (res != 0) return res;
            res = string.Compare(Hash, other.Hash, StringComparison.Ordinal);
            if (res != 0) return res;
            return string.Compare(_LocalPath, other.LocalPath, StringComparison.Ordinal);
        }
        public int CompareTo(object obj)
        {
            return obj is ReleasedFile file ? CompareTo(file) : -1;
        }


        #region Cloneable

        public new ReleasedFile Clone() { return (ReleasedFile)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new ReleasedFile(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (ReleasedFile)source;
            _Year =  from._Year;
            _Month = from._Month;
            _Day = from._Day;
            _Hour = from._Hour;
            _Hash = from._Hash;
            _LocalPath = from._LocalPath;
        }
    
        #endregion
  
    }
}
