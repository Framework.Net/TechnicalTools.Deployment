﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;


namespace TechnicalTools.Deployment.Data
{
    public class DefaultDataAccessor : DbMapperWrapper, IDataAccessor
    {
        public DefaultDataAccessor(Config cfg)
            : this(DbMapperFactory.CreateSqlMapper(new NamedObject(cfg.ApplicationName + " deployment connection"), cfg.InitialConnectionString))
        {

        }
        public DefaultDataAccessor(IDbMapper mapper) : base(mapper)
        {
        }

        public virtual bool IsAuthenticationRequired { get { return false; } }
        public virtual bool IsAuthenticated          { get; set; }

        public virtual List<EnvironmentSetting> Authenticate(EnvironmentConfig env, string login, string password)
        { throw new NotSupportedException($"{nameof(IDataAccessor)}.{nameof(Authenticate)} must be implemented in class \"{GetType().Name}\"!"); }

        T                IDataAccessor.CreateInstance<T>()                                                          { return Activator.CreateInstance<T>(); }
        void             IDataAccessor.CreateInDatabase(IDeploymentData item)                                       { CreateInDatabase(item, false); }
        void             IDataAccessor.CreateInDatabaseCollection<T>(IReadOnlyCollection<T> items)                  { CreateInDatabaseCollection(items, false); }
        long             IDataAccessor.GetFileLength(ReleasedFile file)                                             { return DefaultDataAccessor_ForSql.GetFileLength(this, file); }
        IEnumerable<int> IDataAccessor.DownloadFileByChunk(ReleasedFile file, byte[] uniqueChunk, long totalLength) { return DefaultDataAccessor_ForSql.DownloadFileByChunk(this, file, uniqueChunk, totalLength); }

        int IDataAccessor.UpdateToDatabase<T>(T item) 
        { return base.UpdateToDatabase(item, true); }

        void IDataAccessor.DeleteInDatabase<T>(T item)
        { base.DeleteInDatabase(item, true); }

        List<T> IDataAccessor.LoadCollection<T>() 
        { return base.LoadCollection<T>(); }
    }

    /// <summary>
    /// Help you to implement IDataAccessor if you are :
    /// - using a IDbMapper to connect to a your database 
    /// - Your database is Sql Server
    /// - You keep the default mapping of classes and properties
    /// </summary>
    public static class DefaultDataAccessor_ForSql
    {
        public static long GetFileLength(IDbMapper mapper, ReleasedFile file)
        {
            using (SqlConnection conn = SqlConnectionTracer.Create(mapper.GetConnectionString()))
            {
                conn.OpenOrWaitUntilOpen();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select datalength(" + mapper.GetColumnNameFor((FileWithContent f) => f.Content) + ")"
                                      + " from " + mapper.GetTableMapping(typeof(FileWithContent)).FullName
                                      + " where " + mapper.GetColumnNameFor((FileWithContent f) => f.Hash) + " = " + DbMappedField.ToSql(file.Hash);
                    return (int)(long)cmd.ExecuteScalar();
                }
            }
        }
        public static IEnumerable<int> DownloadFileByChunk(IDbMapper mapper, ReleasedFile file, byte[] uniqueChunk, long total)
        {
            Debug.Assert(uniqueChunk.LongLength <= int.MaxValue);
            using (SqlConnection conn = SqlConnectionTracer.Create(mapper.GetConnectionString()))
            {
                conn.OpenOrWaitUntilOpen();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Select " + mapper.GetColumnNameFor((FileWithContent f) => f.Content)
                                    + " from " + mapper.GetTableMapping(typeof(FileWithContent)).FullName
                                    + " where " + mapper.GetColumnNameFor((FileWithContent f) => f.Hash) + " = " + DbMappedField.ToSql(file.Hash);

                    using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess))
                        if (reader.Read())
                        {
                            var stream = reader.GetStream(0);
                            int cumulatedRead = 0;
                            while (true)
                            {
                                int read = stream.Read(uniqueChunk, 0, uniqueChunk.Length);
                                if (read == 0)
                                    break;
                                cumulatedRead += read;
                                yield return read;
                            }
                        }
                }
            }
        }
    }
}
