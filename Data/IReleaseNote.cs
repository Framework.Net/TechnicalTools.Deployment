﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IReleaseNote : IDeploymentData
    {
            int Id            { get; set; }
        Version Version       { get; set; }
           bool IsPublic      { get; set; }
         string Note          { get; set; }
         string NoteTechnical { get; set; }
           bool HasGif        { get; }
    }
    public interface IReleaseNoteWithContent : IReleaseNote
    {
         byte[] ExplainingGIF { get; set; }
    }
}
