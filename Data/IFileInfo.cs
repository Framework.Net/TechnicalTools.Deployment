﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IFileInfo : IDeploymentData
    {
        string Hash          { get; set; }
        string FileName      { get; set; }
    }
    public interface IFileWithContent : IFileInfo
    {
        byte[] Content       { get; set; }
    }
}
