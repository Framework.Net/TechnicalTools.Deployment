﻿using System;


namespace TechnicalTools.Deployment.Data
{
    public interface IEnvironmentConfig : IDeploymentData
    {
        int          Id                      { get; set; }
        eEnvironment Domain                  { get; set; }
        string       Name                    { get; set; }
        int?         InheritedEnvironment_Id { get; set; }
        bool         IsHiddenFromUser        { get; set; }
    }
}
