﻿namespace TechnicalTools.Deployment
{
    partial class FrmDeployer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.btnInstall = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblLogs = new System.Windows.Forms.Label();
            this.chkRunAfterInstall = new System.Windows.Forms.CheckBox();
            this.chkInstallShortCutOnDesktop = new System.Windows.Forms.CheckBox();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEnvName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(178, 15);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(127, 20);
            this.txtLogin.TabIndex = 0;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(113, 18);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(39, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "Login :";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(113, 49);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(59, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password :";
            // 
            // btnInstall
            // 
            this.btnInstall.Location = new System.Drawing.Point(178, 153);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(127, 23);
            this.btnInstall.TabIndex = 2;
            this.btnInstall.Text = "Install And Run";
            this.btnInstall.UseVisualStyleBackColor = true;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(178, 46);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(127, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(367, 127);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(127, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Leave (invisible)\r\n";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblLogs
            // 
            this.lblLogs.AutoSize = true;
            this.lblLogs.Location = new System.Drawing.Point(21, 236);
            this.lblLogs.Name = "lblLogs";
            this.lblLogs.Size = new System.Drawing.Size(484, 13);
            this.lblLogs.TabIndex = 5;
            this.lblLogs.Text = "log  log  log  log  log  log  log  log  log  log  log  log  log  log  log  log  l" +
    "og  log  log  log  log  log  log  log ";
            // 
            // chkRunAfterInstall
            // 
            this.chkRunAfterInstall.AutoSize = true;
            this.chkRunAfterInstall.Checked = true;
            this.chkRunAfterInstall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRunAfterInstall.Location = new System.Drawing.Point(178, 205);
            this.chkRunAfterInstall.Name = "chkRunAfterInstall";
            this.chkRunAfterInstall.Size = new System.Drawing.Size(155, 17);
            this.chkRunAfterInstall.TabIndex = 6;
            this.chkRunAfterInstall.Text = "Run <your app> after install";
            this.chkRunAfterInstall.UseVisualStyleBackColor = true;
            // 
            // chkInstallShortCutOnDesktop
            // 
            this.chkInstallShortCutOnDesktop.AutoSize = true;
            this.chkInstallShortCutOnDesktop.Checked = true;
            this.chkInstallShortCutOnDesktop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInstallShortCutOnDesktop.Enabled = false;
            this.chkInstallShortCutOnDesktop.Location = new System.Drawing.Point(178, 182);
            this.chkInstallShortCutOnDesktop.Name = "chkInstallShortCutOnDesktop";
            this.chkInstallShortCutOnDesktop.Size = new System.Drawing.Size(222, 17);
            this.chkInstallShortCutOnDesktop.TabIndex = 7;
            this.chkInstallShortCutOnDesktop.Text = "Install Shortcut \"<your app>\" On Desktop";
            this.chkInstallShortCutOnDesktop.UseVisualStyleBackColor = true;
            // 
            // txtDomain
            // 
            this.txtDomain.Enabled = false;
            this.txtDomain.Location = new System.Drawing.Point(178, 88);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.Size = new System.Drawing.Size(100, 20);
            this.txtDomain.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Domain :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Env :";
            // 
            // txtEnvName
            // 
            this.txtEnvName.Enabled = false;
            this.txtEnvName.Location = new System.Drawing.Point(178, 114);
            this.txtEnvName.Name = "txtEnvName";
            this.txtEnvName.Size = new System.Drawing.Size(100, 20);
            this.txtEnvName.TabIndex = 10;
            // 
            // FrmDeployer
            // 
            this.AcceptButton = this.btnInstall;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(517, 347);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEnvName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDomain);
            this.Controls.Add(this.chkInstallShortCutOnDesktop);
            this.Controls.Add(this.chkRunAfterInstall);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblLogs);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnInstall);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblLogin);
            this.Name = "FrmDeployer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<your app> deployment tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblLogs;
        private System.Windows.Forms.CheckBox chkRunAfterInstall;
        private System.Windows.Forms.CheckBox chkInstallShortCutOnDesktop;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEnvName;
    }
}

